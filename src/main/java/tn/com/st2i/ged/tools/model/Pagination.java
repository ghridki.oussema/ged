package tn.com.st2i.ged.tools.model;

import lombok.Data;

@Data
public class Pagination {

	private Integer offSet;
	private Integer limit;

}
