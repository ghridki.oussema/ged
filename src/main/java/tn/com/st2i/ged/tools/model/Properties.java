package tn.com.st2i.ged.tools.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Properties {

	@JsonProperty("cm:title")
	public String cmTitle;
	@JsonProperty("cm:versionType")
	public String cmVersionType;
	@JsonProperty("cm:versionLabel")
	public String cmVersionLabel;
	@JsonProperty("cm:author")
	public String cmAuthor;
	@JsonProperty("cm:description")
	public String cmDescription;

}
