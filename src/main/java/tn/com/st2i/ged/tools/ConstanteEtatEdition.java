package tn.com.st2i.ged.tools;

public class ConstanteEtatEdition {

	public static final String FORMAT_PDF = "pdf";
	public static final String FORMAT_WORD = "word";

	// public static final String PATH_IN_LINUX =
	// "/opt/apache-tomcat-8.5.42/webapps/convocation/";
	public static final String PATH_IN_LINUX = "/usr/share/apache-tomcat-8.5.9/webapps/convocationComm/";
	public static final String PATH_IN_WINDOWS = "C:\\convocationComm\\";
	
	public static final String PATH_FILE = "";
	public static final String PATH = "$$$"; /* WINDOWS \\ || LINUX / */
	
	public static final String PATH_TEST ="C:\\convocation\\Attestations\\attestationAppel.docx";
	public static final String PATH_MODELE_ENREGISTREMENT ="C:\\convocationComm\\modele_enregistrement-req-assignation.docx";
	
}
