package tn.com.st2i.ged.tools.model;

import lombok.Data;

@Data
public class Content {
	
    public String mimeType;
    public String mimeTypeName;
    public int sizeInBytes;
    public String encoding;

}
