package tn.com.st2i.ged.tools.model;

import java.util.List;

import lombok.Data;

@Data
public class SearchObject {

	private Pagination pagination;
	private Sort sort;
	private List<Sort> listSort;
	private List<CriteriaSearch> dataSearch;
	private List<String> listCol;

	public SearchObject(Sort sort) {
		this.sort = sort;
	}

	public SearchObject() {

	}

}
