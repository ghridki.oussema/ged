package tn.com.st2i.ged.tools.model;

import lombok.Data;

@Data
public class CriteriaSearch {

	private String key;
	private String value;
	private String specificSearch;
	
}
