package tn.com.st2i.ged.tools;

public class Constante {

	// ################################################
	// # Actif Account User
	// ################################################

	public static final Long CODE_DISABLE_ACCOUNT = 0L;
	public static final Long CODE_ACTIF_ACCOUNT = 1L;
	public static final Long CODE_DEAVTIVE_ACCOUNT = 2L;

	public static final String CODE_CA = "CA";
	public static final String CODE_CD = "CD";
	public static final String CODE_DOSS = "DOSS";

	// #######################################
	// code Adm Param
	// #######################################

	public static final String _CODE_PATH_FILE = "pathFolder";
	public static final String _CODE_PATH = "path";

	// #######################################
	// code NmTypeDoc
	// #######################################
	public static final String CODE_NM_TYPE_DOC_AUTRE = "AUTRE"; 
	
	
	// #######################################
		// code nmNatureDoc
		// #######################################
	public static final String CODE_NM_NATURE_DOC_ORIGINALE = "ORIGINALE"; 
	
}
