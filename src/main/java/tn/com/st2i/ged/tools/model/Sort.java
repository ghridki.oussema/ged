package tn.com.st2i.ged.tools.model;

import lombok.Data;

@Data
public class Sort {

	private String nameCol = null;
	private String direction = null;
	
}
