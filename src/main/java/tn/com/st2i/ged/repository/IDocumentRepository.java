package tn.com.st2i.ged.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.st2i.ged.model.Document;

@Repository
public interface IDocumentRepository extends JpaRepository<Document, Long> {

	
	
	@Query(value = "select   coalesce(max(CAST(d.num_doc AS INT)), 0) + 1\r\n"
			+ "from application.document d\r\n"
			+ "where d.id_requete =:idRequete\r\n"
			+ "or d.id_projet = ANY(select id from projet p where p.id_requete=:idRequete)", nativeQuery = true)
	Integer findNextNumDocByIdRequete(@Param(value="idRequete") Long idRequete);
	


	
	@Query(value = "select  coalesce(max(CAST(d.num_doc AS INT)), 0) + 1\r\n"
			+ "from application.document d\r\n"
			+ "where d.id_projet=:idProjet\r\n"
			+ "or d.id_requete  = ANY(select id_requete  from projet p where p.id=:idProjet)", nativeQuery = true)
	Integer findNextNumDocByIdProjet(@Param(value="idProjet") Long idProjet);
	
}
