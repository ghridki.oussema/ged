package tn.com.st2i.ged.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.st2i.ged.tools.model.SendObject;
import tn.com.st2i.ged.service.IDocService;
import tn.com.st2i.ged.service.IUploadFileService;
import tn.com.st2i.ged.tools.ConstanteWs;
import tn.com.st2i.ged.tools.UtilsWs;

@Service
public class DocService implements IDocService {

	private static final Logger logger = LogManager.getLogger(UploadFileService.class);

	@Autowired
	private IUploadFileService uploadFileService;

	@Autowired
	private UtilsWs utilsWs;

	@Override
	public SendObject downloadFileFromAlfrByNodeRef(String nodeRef) {
		try {
			if (nodeRef == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			return this.uploadFileService.handleFileDownload(nodeRef);
		} catch (Exception e) {
			logger.error("Error DocService in method downloadFileFromAlfrByNodeRef :: " + e.toString());
			return new SendObject(ConstanteWs._CODE_WS_ERROR_IN_METHOD, null);
		}
	}

	@Override
	public SendObject deleteFileWs(String nodeRef) {
		try {
			if (nodeRef == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			SendObject so = this.uploadFileService.handleFileToDeleteByRefNode(nodeRef);
			return utilsWs.resultWs(so.getCode(), so.getPayload());
		} catch (Exception e) {
			logger.error("Error DocService in method deleteFileWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteListFileWs(List<String> list) {
		try {
			if (list == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			SendObject so = this.uploadFileService.handleFileToDeleteListNodeRef(list);
			return utilsWs.resultWs(so.getCode(), so.getPayload());
		} catch (Exception e) {
			logger.error("Error DocService in method deleteListFileWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject downloadFolderFromAlfrByNodeRef(String nodeRef, String fileName) {
		try {
			if (nodeRef == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			return this.uploadFileService.handleFolderDownload(nodeRef, fileName);
		} catch (Exception e) {
			logger.error("Error DocService in method downloadFolderFromAlfrByNodeRef :: " + e.toString());
			return new SendObject(ConstanteWs._CODE_WS_ERROR_IN_METHOD, null);
		}
	}

	@Override
	public SendObject deleteFolder(String nodeRef) {
		try {
			if (nodeRef == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			SendObject so = this.uploadFileService.deleteFolder(nodeRef);
			return utilsWs.resultWs(so.getCode(), so.getPayload());
		} catch (Exception e) {
			logger.error("Error DocService in method deleteFileWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
