package tn.com.st2i.ged.service;

import java.util.List;

import tn.com.st2i.ged.tools.model.SendObject;

public interface IDocService {

	
	public SendObject downloadFileFromAlfrByNodeRef(String nodeRef);
	
	public SendObject downloadFolderFromAlfrByNodeRef(String nodeRef,String fileName);

	public SendObject deleteFileWs(String nodeRef);

	public SendObject deleteListFileWs(List<String> list);
	
	public SendObject deleteFolder(String nodeRef);
}
