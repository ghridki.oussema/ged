package tn.com.st2i.ged.service.impl;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import tn.com.st2i.ged.model.Document;
import tn.com.st2i.ged.model.RefDocument;
import tn.com.st2i.ged.tools.model.SendObject;
import tn.com.st2i.ged.repository.IDocumentRepository;
import tn.com.st2i.ged.service.ICommonService;
import tn.com.st2i.ged.service.IDocumentService;
import tn.com.st2i.ged.service.IUploadFileService;
import tn.com.st2i.ged.tools.ConstanteService;
import tn.com.st2i.ged.tools.ConstanteWs;
import tn.com.st2i.ged.tools.UtilsWs;

@Service
public class DocumentService implements IDocumentService {

	private static final Logger logger = LogManager.getLogger(DocumentService.class);

	@Autowired
	private IDocumentRepository documentRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Autowired
	private IUploadFileService uploadFileService;

	@Override
	public List<Document> getList() {
		try {
			return documentRepository.findAll();
		} catch (Exception e) {
			logger.error("Error DocumentService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public List<RefDocument> getListDocByRefDoss(String refNode) {
		try {
			return uploadFileService.getListDocByRefDoss(refNode);
		} catch (Exception e) {
			logger.error("Error DocumentService in method getListDocByRefDoss :: " + e.toString());
			return null;
		}
	}

	@Override
	public Document findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new Document(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS)) {
				return (Document) sendObject.getPayload();
			}
			return null;
		} catch (Exception e) {
			logger.error("Error DocumentService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public Document saveOrUpdate(Document entity) {
		try {
			Timestamp dtNow = (Timestamp) this.commonService.getDateSystemNow().getPayload();
			entity.setDtEnr(dtNow);
			entity.setDtMaj(dtNow);
			return documentRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error DocumentService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			documentRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error DocumentService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findDocumentByIdWs(Long id) {
		SendObject so = null;
		try {
			if (id == null) {
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			}
			Document entity = this.findById(id);
			if (entity == null) {
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			}

			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error DocumentService in method findDocumentByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListDocumentWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error DocumentService in method getListDocumentWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	public void deleteFileAfterInsert(File file, Document document) {
		try {
			TimerTask repeatedTask = new TimerTask() {
				public void run() {
					try {
						Files.deleteIfExists(Paths.get(file.getPath()));
					} catch (Exception arg) {
						logger.error("Error DocumentService in method generateurDocumentForEtatEdition delete file "
								+ arg.toString());
					}
				}
			};
			Timer timer = new Timer("Timer");
			long delay = 0L;
			long period = 2000L;
			timer.scheduleAtFixedRate(repeatedTask, delay, period);
		} catch (Exception arg1) {
			logger.error(
					"Error DocumentService in method generateurDocumentForEtatEdition delete file " + arg1.toString());
		}
	}

	public Document initDocument(Document document) {
		SendObject so = this.commonService.getDateSystemNow();
		document.setDtEnr((Timestamp) so.getPayload());
		document.setDtMaj((Timestamp) so.getPayload());
		return document;
	}

	@Override
	public SendObject saveFile(Document document, List<MultipartFile> listFile) {
		SendObject so = new SendObject();
		try {
			
			if(document.getId() == null) {
				document.setDtEnr(new Date());
				document.setDtMaj(new Date());
				
				if(document.getIdRequete() !=null) {
					document.setNumDoc(this.documentRepository.findNextNumDocByIdRequete(document.getIdRequete()).toString());
					System.out.println("aaa"+document);
				} else {
					document.setNumDoc(this.documentRepository.findNextNumDocByIdProjet(document.getIdProjet()).toString());
				}
				
			} else {
				document.setDtMaj(new Date());
			}  
if(listFile == null || listFile.isEmpty()) {
				
				document.setIdNodeRef(null);
				document = this.documentRepository.save(document);
				if(document == null){
					return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
				}
				return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(document));
			}

			document = this.documentRepository.save(document);

			if (document.getIdProjet() != null && document.getIdConvention() == null
					&& document.getIdFinancement() != null)

				so = this.uploadFileService.handleUploadFile(listFile,
						document.getIdFinancement().toString() + "/" + document.getId().toString(), "Financement");

			else if (document.getIdProjet() != null && document.getIdConvention() != null
					&& document.getIdFinancement() != null)

				so = this.uploadFileService.handleUploadFile(listFile,
						document.getIdConvention().toString() + "/" + document.getId().toString(), "Convention");

			else if (document.getIdProjet() != null && document.getIdConvention() == null
					&& document.getIdFinancement() == null)
				so = this.uploadFileService.handleUploadFile(listFile,
						document.getIdProjet().toString() + "/" + document.getId().toString(), "Projet");

			else if (document.getIdRequete() != null && document.getIdProjet() == null
					&& document.getIdConvention() == null && document.getIdFinancement() == null)
				so = this.uploadFileService.handleUploadFile(listFile,
						document.getIdRequete().toString() + "/" + document.getId().toString(), "Requete");

			if (!so.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());

			document.setIdNodeRef((String) so.getPayload());
			document = this.documentRepository.save(document);

			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(document));
		} catch (Exception e) {
			logger.error("Error DocumentService in method saveFile() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	public Optional<String> getExtensionByStringHandling(String filename) {
		return Optional.ofNullable(filename).filter(f -> f.contains("."))
				.map(f -> f.substring(filename.lastIndexOf(".") + 1));
	}

	@Override
	public SendObject handleListDocByRefDoss(String refNode) {
		try {
			SendObject so = uploadFileService.handleListDocByRefDoss(refNode);
			if(!so.getCode().equals(ConstanteWs._CODE_WS_SUCCESS))
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, so.getPayload());
		} catch (Exception e) {
			logger.error("Error DocumentService in method findDocumentByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
