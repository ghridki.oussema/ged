package tn.com.st2i.ged.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import tn.com.st2i.ged.model.Document;
import tn.com.st2i.ged.model.RefDocument;
import tn.com.st2i.ged.tools.model.SendObject;

public interface IDocumentService {

	public List<Document> getList();

	public Document findById(Long id);

	public Document saveOrUpdate(Document entity);

	public Boolean deleteById(Long id);

	public SendObject findDocumentByIdWs(Long id);

	public SendObject getListDocumentWs();

	public SendObject saveFile(Document entity, List<MultipartFile> filedata);
	
	public SendObject handleListDocByRefDoss(String refNode);
	
	public List<RefDocument> getListDocByRefDoss(String refNode);




}
