package tn.com.st2i.ged.service.impl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import tn.com.st2i.ged.model.FileSend;
import tn.com.st2i.ged.model.RefDocument;
import tn.com.st2i.ged.tools.model.SendObject;
import tn.com.st2i.ged.service.IUploadFileService;
import tn.com.st2i.ged.tools.ConstanteService;

@Service
public class UploadFileService implements IUploadFileService {

	private static final Logger logger = LogManager.getLogger(UploadFileService.class);

	@Autowired
	private CmisConnector cmisConnector;

	@Override
	public SendObject handleUploadFile(List<MultipartFile> listeFile, String ref, String folder) {
		try {
			Map<String, Object> props = new HashMap<String, Object>();
			String[] arrOfStr = ref.split("/", 0);
			props.put("folder", folder);
			props.put("ref", ref);
			props.put("cm:title", ref);

			return handleUploadFileResult(listeFile, arrOfStr[1], props);
		} catch (Exception argEx) {
			logger.error("Error handleUploadFile in method handleUploadFile " + argEx.toString());
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR_IN_METHOD);
		}
	}

	public SendObject handleUploadFileResult(List<MultipartFile> listeFile, String attachementName,
			Map<String, Object> props) {
		String nodeRef = null;
		try {
			Folder parentFolder = cmisConnector.createFolder(attachementName, props);
			nodeRef = parentFolder.getId();
			if (listeFile == null)
				return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS, nodeRef);
			for (MultipartFile f : listeFile) {
				String ext = "." + FilenameUtils.getExtension(f.getOriginalFilename());
				InputStream targetStream = new BufferedInputStream(f.getInputStream());
				Document doc = cmisConnector.createDocument(parentFolder, f.getOriginalFilename(), targetStream,
						f.getSize(), ext, null);
				System.err.println("event id " + doc.getId());
			}
			return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS, nodeRef);
		} catch (Exception argEx) {
			logger.error("Error handleUploadFile in method handleUploadFileResult " + argEx.toString());
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR_IN_METHOD);
		}
	}

	@Override
	public SendObject handleFileDownload(String refNode) throws Exception {
		try {
			Document document = (Document) cmisConnector.getObjectByNodeRef(refNode);
			ContentStream contentStream = document.getContentStream();
			InputStream is = contentStream.getStream();
			byte[] bytes = IOUtils.toByteArray(is);
			File tempDir = new File(System.getProperty("java.io.tmpdir"));
			File file = new File(tempDir + File.separator + new Date().getTime() + " " + document.getName());
			FileUtils.writeByteArrayToFile(file, bytes);
			return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS, file);
		} catch (Exception e) {
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR_IN_METHOD, null);
		}
	}

	public SendObject handleUploadFileResult(List<FileSend> listeFile, Map<String, Object> props) {
		try {
			Folder parentFolder = cmisConnector.createFolder(null, props);
			List<FileSend> listResult = new ArrayList<FileSend>();

			for (FileSend fs : listeFile) {
				String encodedString = Base64.getEncoder().encodeToString(fs.getFileContent());

				File tempDir = new File(System.getProperty("java.io.tmpdir"));
				File f = new File(tempDir + File.separator + new Date().getTime() + File.separator + fs.getFileName());

				byte[] decodedBytes = Base64.getDecoder().decode(encodedString);
				FileUtils.writeByteArrayToFile(f, decodedBytes);

				String ext = ".111"; // "." + FilenameUtils.getExtension(f.getName());
				InputStream targetStream = new BufferedInputStream(new FileInputStream(f));
				Document doc = cmisConnector.createDocument(parentFolder, fs.getNewFileName(), targetStream, f.length(),
						ext, null);
				fs.setNodeRef(doc.getId());
				fs.setFileContent(null);
				listResult.add(fs);
			}

			return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS, listResult);
		} catch (Exception argEx) {
			logger.error("Error handleUploadFile in method handleUploadFileResult " + argEx.toString());
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR_IN_METHOD);
		}
	}

	@Override
	public SendObject handleFileToDeleteByRefNode(String refNode) {
		try {
			this.cmisConnector.deleteFile(refNode);
			return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS, null);
		} catch (Exception argEx) {
			logger.error("Error handleUploadFile in method handleFileToDeleteByRefNode " + argEx.toString());
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR_IN_METHOD);
		}
	}

	@Override
	public SendObject handleFileToDeleteListNodeRef(List<String> list) {
		try {
			for (String refNode : list)
				this.cmisConnector.deleteFile(refNode);
			return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS, null);
		} catch (Exception argEx) {
			logger.error("Error handleUploadFile in method handleFileToDeleteListNodeRef " + argEx.toString());
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR_IN_METHOD);
		}
	}

	@Override
	public SendObject handleUploadFileNew(MultipartFile file, String path) {
		try {
			path.trim();
			path = path.replaceAll("//", "/");
			List<String> str = Arrays.asList(path.split("/"));
			List<String> list = new ArrayList<String>();
			String fileNameF = "";
			for (int i = 0; i < str.size(); i++) {
				if (i == 0) {
					if (!str.get(0).equals(""))
						list.add(str.get(i));
				} else if (i != str.size() - 1)
					list.add(str.get(i));
				else
					fileNameF = str.get(i);
			}
			Folder parentFolder = this.cmisConnector.createFolder(fileNameF, list);

			String ext = "." + FilenameUtils.getExtension(fileNameF);
			InputStream targetStream = new BufferedInputStream(file.getInputStream());
			Document doc = cmisConnector.createDocument(parentFolder, fileNameF, targetStream, file.getSize(), ext,
					null);

			return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS, doc.getId());
		} catch (Exception argEx) {
			logger.error("Error handleUploadFile in method handleUploadFile " + argEx.toString());
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR_IN_METHOD);
		}
	}

	@Override
	public SendObject handleFolderToDeleteListNodeRef(List<String> list) {
		try {

			return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS, list);
		} catch (Exception argEx) {
			logger.error("Error handleUploadFile in method handleFolderToDeleteListNodeRef " + argEx.toString());
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR_IN_METHOD);
		}
	}

	@Override
	public SendObject handleFolderDownload(String refNode, String fileName) throws Exception {
		try {
			Document document = (Document) cmisConnector.getObjectByRefDoss(refNode, fileName);
			ContentStream contentStream = document.getContentStream();
			InputStream is = contentStream.getStream();
			byte[] bytes = IOUtils.toByteArray(is);
			File tempDir = new File(System.getProperty("java.io.tmpdir"));
			File file = new File(tempDir + File.separator + new Date().getTime() + " " + document.getName());
			FileUtils.writeByteArrayToFile(file, bytes);
			return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS, file);
		} catch (Exception e) {
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR_IN_METHOD, null);
		}
	}

	@Override
	public SendObject handleListDocByRefDoss(String refNode) throws Exception {
		try {

			return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS,
					new JSONArray(this.cmisConnector.getListDocByRefDoss(refNode)));
		} catch (Exception argEx) {
			logger.error("Error handleUploadFile in method handleListDocByRefDoss " + argEx.toString());
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR_IN_METHOD);
		}

	}

	@Override
	public SendObject deleteFolder(String nodeRef) {
		try {
			return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS, this.cmisConnector.deleteFolder(nodeRef));

		} catch (Exception argEx) {
			logger.error("Error handleUploadFile in method deleteFolder " + argEx.toString());
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR_IN_METHOD);
		}

	}

	@Override
	public List<RefDocument> getListDocByRefDoss(String refNode) {
		try {
			return this.cmisConnector.getListDocByRefDoss(refNode);
		} catch (Exception e) {
			logger.error("Error handleUploadFile in method getListDocByRefDoss " + e.toString());
			return null;
		}

	}

}
