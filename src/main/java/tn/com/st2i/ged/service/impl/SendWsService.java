package tn.com.st2i.ged.service.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import tn.com.st2i.ged.model.UmUser;
import tn.com.st2i.ged.tools.model.SendObject;
import tn.com.st2i.ged.service.ISendWsService;
import tn.com.st2i.ged.tools.ConstanteService;

@Service
public class SendWsService implements ISendWsService {
	
	@Value("${spring.application.name}")
	private String nameService;
	 
	 @Autowired
	private RestTemplate restTemplate;

	private static final Logger logger = LogManager.getLogger(SendWsService.class);

	@Override
	public ResponseEntity<?> sendResult(HttpServletRequest request, SendObject so) {
		try {
			return new ResponseEntity<>(so.getPayload().toString(), new HttpHeaders(), so.getHttp());
		} catch (Exception argEx) {
			logger.error("Error SendWsService in method sendResult :: " + argEx.toString());
			return new ResponseEntity<>(so.getPayload().toString(), new HttpHeaders(), so.getHttp());
		}
	}

	@Override
	public ResponseEntity<?> sendResultException(HttpServletRequest request) {
		return new ResponseEntity<>(new JSONObject(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	public ResponseEntity<?> sendResultPublic(HttpServletRequest request, SendObject so) {
		return new ResponseEntity<>(so.getPayload().toString(), new HttpHeaders(), so.getHttp());
	}

	@Override
	public String getCurrentUserId(HttpServletRequest request) {
		try {
			ResponseEntity<SendObject> os = restTemplate.postForEntity("http://gateway-service/gateway/intern/current/user/id", request.getHeader("Authorization"), SendObject.class);
			SendObject so = os.getBody();
			if (!so.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return null;
			return (String) so.getPayload();
		} catch (Exception argEx) {
			logger.error("Error SendWsService in method getCurrentUserId :: " + argEx.toString());
			return null;
		}
	}

	@Override
	public UmUser getCurrentUser(HttpServletRequest request) {
		UmUser umUser = new UmUser();
		try {
			ResponseEntity<SendObject> os = restTemplate.postForEntity("http://gateway-service/gateway/intern/current/user", request.getHeader("Authorization"), SendObject.class);
			SendObject so = os.getBody();
			if (!so.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return null;
			ObjectMapper mapper = new ObjectMapper();
			umUser = mapper.convertValue(so.getPayload(), UmUser.class);
			return umUser;
		} catch (Exception argEx) {
			logger.error("Error SendWsService in method getCurrentUser :: " + argEx.toString());
			return null;
		}
	}


}
