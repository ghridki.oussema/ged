package tn.com.st2i.ged.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import tn.com.st2i.ged.model.RefDocument;
import tn.com.st2i.ged.tools.model.SendObject;

public interface IUploadFileService {

	public SendObject handleUploadFile(List<MultipartFile> listeFile, String ref, String subFolder);

	public SendObject handleFileDownload(String refNode) throws Exception;
	
	public SendObject handleFolderDownload(String refNode,String fileName) throws Exception;
	
	public SendObject handleListDocByRefDoss(String refNode) throws Exception;

	public SendObject handleFileToDeleteByRefNode(String refNode);

	public SendObject handleFileToDeleteListNodeRef(List<String> list);

	public SendObject handleFolderToDeleteListNodeRef(List<String> list);

	public SendObject handleUploadFileNew(MultipartFile file, String path);
	
	public SendObject deleteFolder(String nodeRef);
	
	public List<RefDocument> getListDocByRefDoss(String refNode);

}
