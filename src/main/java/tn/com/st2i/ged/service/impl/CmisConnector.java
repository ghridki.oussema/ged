package tn.com.st2i.ged.service.impl;

import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.FileableCmisObject;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.OperationContext;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.Repository;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.api.Tree;
import org.apache.chemistry.opencmis.client.runtime.OperationContextImpl;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.data.PropertyData;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.UnfileObject;
import org.apache.chemistry.opencmis.commons.exceptions.CmisContentAlreadyExistsException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import tn.com.st2i.ged.model.RefDocument;
import tn.com.st2i.ged.service.ICommonService;

@Service
public class CmisConnector {

	private static final Logger logger = LogManager.getLogger(CmisConnector.class.getName());

	@Value("${cmis.url}")
	private String url;

	@Value("${cmis.user}")
	private String user;

	@Value("${cmis.password}")
	private String password;

	private Session cmisSession = null;

	@Autowired
	private ICommonService commonService;

	public Folder createFolder(String attachementName, Map<String, Object> props) {

		ArrayList<String> path = new ArrayList<String>();
		Timestamp dtNow = (Timestamp) this.commonService.getDateSystemNow().getPayload();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dtNow);
		path.add(calendar.get(Calendar.YEAR) + "");
		java.text.SimpleDateFormat df1 = new java.text.SimpleDateFormat("MM");
		path.add(df1.format(calendar.getTime()) + "");
		path.add(props.get("folder").toString());
		String[] arrOfStr = props.get("ref").toString().split("/", 0);
		path.add(arrOfStr[0]);
		path.add(attachementName);
		props.remove("folder");
		props.remove("ref");
		int n = path.size();
		String[] parentPath = new String[n];
		for (int i = 0; i < n; i++) {
			parentPath[i] = path.get(i);
		}
		return createFolder(parentPath, props, "");
	}

	public Folder createFolder(String[] folderPath, Map<String, Object> props, String attachementName) {
		Folder parentFolder = (Folder) getObjectByPath("/");
		for (int i = 0; i < folderPath.length; i++) {
			if (i != folderPath.length)
				parentFolder = createFolder(parentFolder, folderPath[i], null);
		}
//		return parentFolder;
		if (props.containsKey("modEdit"))
			if ((boolean) props.get("modEdit"))
				return updateNameFolder(parentFolder, attachementName, props);
		return createFolder(parentFolder, attachementName, props);
	}

	/**
	 * createFolder
	 * 
	 * @param parentFolder
	 * @param folderName
	 * @param props
	 * @return
	 */
	public Folder createFolder(Folder parentFolder, String attachementName, Map<String, Object> props) {
		Session cmisSession = getSession();
		logger.info("cmis:createFolder:::::::::::::" + attachementName);
		Folder subFolder = null;
		try {
			String parentPath = parentFolder.isRootFolder() ? "" : parentFolder.getPath();
			subFolder = (Folder) cmisSession.getObjectByPath(parentPath + "/" + attachementName);
			logger.info("Folder already existed!");
		} catch (CmisObjectNotFoundException onfe) {
			// create a map of properties if one wasn't passed in
//			if (props == null) {
			props = new HashMap<String, Object>();
//			}
			// Add the object type ID if it wasn't already
			if (props.get("cmis:objectTypeId") == null) {
				props.put("cmis:objectTypeId", "cmis:folder");
			}
			// Add the name if it wasn't already
			if (props.get("cmis:name") == null) {
				props.put("cmis:name", attachementName);
			}
			subFolder = parentFolder.createFolder(props);
			String subFolderId = subFolder.getId();
			logger.info("Created new folder: " + subFolderId);
		}
		cmisSession.clear();
		return subFolder;
	}

	/**
	 * getObject
	 * 
	 * @param id
	 * @return
	 */
	public CmisObject getObjectById(String id) {
		Session cmisSession = getSession();
		return cmisSession.getObject(cmisSession.createObjectId(id));
	}

	public CmisObject getObjectByRefDoss(String nodeRef, String fileName) {
		Session cmisSession = getSession();
		Folder parentFolder = (Folder) cmisSession.getObject(cmisSession.createObjectId(nodeRef));
		return (cmisSession.getObjectByPath(parentFolder.getPath() + "/" + fileName) != null
				? cmisSession.getObjectByPath(parentFolder.getPath() + "/" + fileName)
				: null);
	}

	public CmisObject getObjectByNodeRef(String nodeRef) {
		Session cmisSession = getSession();
		return cmisSession.getObject(cmisSession.createObjectId(nodeRef));
	}

	public List<RefDocument> getListDocByRefDoss(String nodeRef) {
		List<RefDocument> listeFilesName = new ArrayList<>();
		Session cmisSession = getSession();
		Folder parentFolder = (Folder) cmisSession.getObject(cmisSession.createObjectId(nodeRef));
		List<Tree<FileableCmisObject>> list = this.getDescendents(parentFolder);
		for (Tree<FileableCmisObject> t : list) {
			Document d = (Document) t.getItem();
			listeFilesName.add(new RefDocument(d.getId(), d.getName()));
		}
		return listeFilesName;
	}

	/**
	 * Delete File
	 * 
	 * @param nodeRef
	 */
	public void deleteFile(String nodeRef) {
		Session cmisSession = getSession();
		// RefDocument refDoc = null;
		this.cmisSession.delete(cmisSession.getObject(cmisSession.createObjectId(nodeRef)));
	}

	public boolean deleteFile(String nodeRef, String nameFile) throws Exception {
		Session cmisSession = getSession();
		Folder parentFolder = (Folder) cmisSession.getObject(cmisSession.createObjectId(nodeRef));
		List<Tree<FileableCmisObject>> list = this.getDescendents(parentFolder);
		for (Tree<FileableCmisObject> t : list) {
			Document d = (Document) t.getItem();
			if (d.getName().equals(nameFile)) {
				d.delete();
				return true;
			}
		}
		return false;
	}

	/**
	 * creatDocument
	 * 
	 * @param parentFolder
	 * @param fileName
	 * @param inputStream
	 * @param size
	 * @param mimetype
	 * @param props
	 * @return
	 */
	public Document createDocument(Folder parentFolder, String fileName, InputStream inputStream, Long size,
			String mimetype, Map<String, Object> props) {
		Session cmisSession = getSession();

		// create a map of properties if one wasn't passed in
		if (props == null) {
			props = new HashMap<String, Object>();
//			props.put(SessionParameter.OBJECT_FACTORY_CLASS, "org.alfresco.cmis.client.impl.AlfrescoObjectFactoryImpl");
		}

		// Add the object type ID if it wasn't already
		if (props.get("cmis:objectTypeId") == null) {
			props.put("cmis:objectTypeId", "cmis:document");

		}

		// Add the name if it wasn't already
		if (props.get("cmis:name") == null) {
			props.put("cmis:name", fileName);
		}

		// Add Description
		if (props.get("cmis:description") == null) {
			props.put("cmis:description", "test description");
		}

		// ADD Title
		List<String> secondary = new ArrayList<>();
		secondary.add("P:cm:titled");
		if (props.get("cm:title") == null) {
			props.put(PropertyIds.SECONDARY_OBJECT_TYPE_IDS, secondary);
			props.put("cm:title", fileName);
		}

		// set contentStream object
		ContentStream contentStream = cmisSession.getObjectFactory().createContentStream(fileName, size, mimetype,
				inputStream);

		// process the upload
		Document document = null;
		try {
			document = parentFolder.createDocument(props, contentStream, null);
			logger.info("Created new document: " + document.getId());
		} catch (CmisContentAlreadyExistsException ccaee) {
			document = (Document) cmisSession.getObjectByPath(parentFolder.getPath() + "/" + fileName);
			logger.info("Document already exists: " + fileName);
		}
		return document;
	}

	/**
	 * getObjectByPath
	 * 
	 * @param objectPath
	 * @return
	 */
	public CmisObject getObjectByPath(String objectPath) {
		Session cmisSession = getSession();
		return cmisSession.getObjectByPath(objectPath);
	}

	/**
	 * getSession
	 * 
	 * @return
	 */
	public Session getSession() {
		try {
			if (cmisSession == null) {
				SessionFactory factory = SessionFactoryImpl.newInstance();
				HashMap<String, String> parameter = new HashMap<>();
				parameter.put(SessionParameter.ATOMPUB_URL, url);
				parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
				parameter.put(SessionParameter.AUTH_HTTP_BASIC, "true");
				parameter.put(SessionParameter.USER, user);
				parameter.put(SessionParameter.PASSWORD, password);

//				Set the alfresco object factory Used when using the CMIS extension for Alfresco for working with  aspects
//				parameter.put(SessionParameter.OBJECT_FACTORY_CLASS,
//						"org.alfresco.cmis.client.impl.AlfrescoObjectFactoryImpl");

				List<Repository> repositories = factory.getRepositories(parameter);
				cmisSession = repositories.get(0).createSession();
			}
		} catch (Exception e) {
			logger.error("Error CmisConnector :: getSession " + e.toString());
		}
		return cmisSession;
	}

	/**
	 * getFolderTree
	 * 
	 * @param id
	 * @return
	 */
	public List<Tree<FileableCmisObject>> getFolderTree(Folder parentFolder) {
		Session cmisSession = getSession();
		if (parentFolder == null)
			parentFolder = cmisSession.getRootFolder();
		return parentFolder.getFolderTree(-1); // getFolderTree
	}

	/**
	 * verifExistsFile
	 * 
	 * @param nodeRef
	 * @param nameFile
	 * @return boolean
	 */
	public boolean verifExistsFile(String nodeRef, String nameFile) throws Exception {
		Folder parentFolder = (Folder) cmisSession.getObject(cmisSession.createObjectId(nodeRef));
		List<Tree<FileableCmisObject>> list = this.getDescendents(parentFolder);
		for (Tree<FileableCmisObject> t : list) {
			Document d = (Document) t.getItem();
			if (d.getName().equals(nameFile))
				return true;
		}
		return false;
	}

	/**
	 * getDescendents Tree
	 * 
	 * @param id
	 * @return
	 * @return
	 */
	public List<Tree<FileableCmisObject>> getDescendents(Folder parentFolder) {
		Session cmisSession = getSession();
		if (parentFolder == null)
			parentFolder = cmisSession.getRootFolder();
		return parentFolder.getDescendants(1); // getFolder children
	}

	/**
	 * addLink
	 * 
	 * @param uploadedDocument
	 * @param parentFolderForShortcut
	 * @param props
	 * @return
	 */
	public String addLink(Document uploadedDocument, Folder parentFolderForShortcut, Map<String, Object> props) {
		String link = null;
		Session cmisSession = getSession();
		// create a map of properties if one wasn't passed in
		if (props == null) {
			props = new HashMap<String, Object>();
			props.put(PropertyIds.BASE_TYPE_ID, BaseTypeId.CMIS_ITEM.value());
			// Define a name and description for the link
			props.put(PropertyIds.NAME, "lienDu_" + uploadedDocument.getName());
			props.put("cmis:description", uploadedDocument.getDescription());
			props.put(PropertyIds.OBJECT_TYPE_ID, "I:app:filelink");

			// Define the destination node reference
			String[] nodeRef = uploadedDocument.getId().split(";");
			props.put("cm:destination", "workspace://SpacesStore/" + nodeRef[0]);
		}
		link = cmisSession.createItem(props, parentFolderForShortcut).toString();

		return link;
	}

	/**
	 * Delete Folder
	 * 
	 * @param nodeRef
	 */
	public String deleteFolder(String nodeRef) throws Exception {
		try {
			Session cmisSession = getSession();
			Folder parentFolder = (Folder) cmisSession.getObject(cmisSession.createObjectId(nodeRef));
			parentFolder.deleteTree(true, UnfileObject.DELETE, true);
			return "200";
		} catch (Exception e) {
			return "420";
		}
	}

	/**
	 * 
	 * @param parentFolder
	 * @param attachementName
	 * @param props
	 * @return Folder
	 */
	private Folder updateNameFolder(Folder parentFolder, String attachementName, Map<String, Object> props) {
		String oldAttachementName = (String) props.get("oldAttachementName");
		logger.info("cmis:renameFolder:::::::::::::" + oldAttachementName + " to " + attachementName);
		Folder subFolder = null;
		String parentPath = parentFolder.isRootFolder() ? "" : parentFolder.getPath();
		subFolder = (Folder) cmisSession.getObjectByPath(parentPath + "/" + oldAttachementName);
		subFolder.rename(attachementName);
		return subFolder;
	}

	/**
	 * doQuery
	 * 
	 * @param cql
	 * @param limit
	 * @param offset
	 * @return
	 */
	public Map<String, Object> doQuery(String cql, int maxItemsPerPage, int skipCount) {
		Map<String, Object> pageResult = new HashMap<>();

		Session cmisSession = getSession();
		OperationContext oc = new OperationContextImpl();
		oc.setMaxItemsPerPage(maxItemsPerPage);

		ItemIterable<QueryResult> results = cmisSession.query(cql, false, oc);
		ItemIterable<QueryResult> page = results.skipTo(skipCount).getPage();
		List<CmisObject> listCmisObject = new ArrayList<CmisObject>(0);

		//
		if (page != null) {
			Iterator<QueryResult> pageItems = page.iterator();
			while (pageItems.hasNext()) {
				QueryResult result = pageItems.next();
				for (PropertyData<?> prop : result.getProperties()) {
					String objectId = result.getPropertyValueByQueryName(prop.getQueryName());
					CmisObject cmisObject = getObjectById(objectId);
					if (cmisObject != null) {
						listCmisObject.add(cmisObject);
					}
				}
			}
		}

		// set total records count
		pageResult.put("count", results.getTotalNumItems());
		pageResult.put("data", listCmisObject);

		return pageResult;
	}

	public List<CmisObject> getPagingChildren(Folder parentFolder, int startpage, int numitmesperpage) {
		Session cmisSession = getSession();
		List<CmisObject> list = null;
		if (parentFolder == null)
			parentFolder = cmisSession.getRootFolder();
		ItemIterable<CmisObject> children = parentFolder.getChildren();
		ItemIterable<CmisObject> page = children.skipTo(numitmesperpage * startpage).getPage();
		if (page != null) {
			Iterator<CmisObject> pageItems = page.iterator();

			list = new ArrayList<CmisObject>();
			while (pageItems.hasNext()) {
				CmisObject item = pageItems.next();
				list.add(item);
			}
		}
		return list;
	}

	public Session getCmisSession() {
		return this.cmisSession;
	}

	public void setCmisSession(Session cmisSession) {
		this.cmisSession = cmisSession;
	}

	public Folder getRootFolder() {
		Session cmisSession = getSession();
		return cmisSession.getRootFolder();
	}

	public OperationContext createOperationContext() {
		Session cmisSession = getSession();
		return cmisSession.createOperationContext();

	}

	public Folder createFolder(String attachementName, List<String> path) {
		Map<String, Object> props = new HashMap<String, Object>();
		props.put("cm:description", "");
		props.put("cm:title", "");

		int n = path.size();
		String[] parentPath = new String[n];
		for (int i = 0; i < n; i++) {
			parentPath[i] = path.get(i);
		}
		return createFolder(parentPath, props, path.get(n - 1));
	}

}
