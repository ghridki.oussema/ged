package tn.com.st2i.ged.model;

import lombok.Data;

@Data
public class FileSend {

	private String fileName;
	private String newFileName;
	private byte[] fileContent;
	private String contentType;
	private String nodeRef;
}
