package tn.com.st2i.ged.model;

import lombok.Data;

@Data
public class UmUser {
	
	private Long umUserId;

	private String userId;

	private String userCl;

	private String masterCd;

	private String staffNm;

	private String positionNm;

	private String staffDeptNm;

	private String mtelNo;

	private String telNo;

	private String faxNo;

	private String email;

	private String delFl;

	private String regDt; //Timestamp

	private String modDt; //Timestamp

	private String testFl;

}
