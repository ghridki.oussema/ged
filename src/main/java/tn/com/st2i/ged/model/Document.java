package tn.com.st2i.ged.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "document", schema = "application")

public class Document implements java.io.Serializable, Cloneable {
	
	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "application.seq_document", name = "application.seq_document")
	@GeneratedValue(generator = "application.seq_document", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
	private Long id;

	@Column(name = "id_requete", nullable = false, precision = 22, scale = 0)
	private Long idRequete;
	
	@Column(name = "id_projet")
	private Long idProjet;
	
	@Column(name = "id_convention")
	private Long idConvention;
	
	@Column(name = "id_financement")
	private Long idFinancement;

	@Column(name = "id_node_ref", length = 150)
	private String idNodeRef;
	
	@Column(name = "id_adm_user", nullable = false, precision = 22, scale = 0)
	private Long idAdmUser;

	@Column(name = "id_nm_type_doc", nullable = false, precision = 22, scale = 0)
	private Long idNmTypeDoc;

	@Column(name = "nom_fichier", nullable = false, length = 150)
	private String nomFichier;

	@Column(name = "observation", length = 250)
	private String observation;

	@Column(name = "dt_enr", nullable = false, length = 13)
	private Date dtEnr;

	@Column(name = "dt_maj", nullable = false, length = 13)
	private Date dtMaj;

	@Column(name = "num_doc", length = 50)
	private String numDoc;
	
	@Transient
	private List<MultipartFile> listFile;

	public Document clone() throws CloneNotSupportedException {
		return (Document) super.clone();
	}
}

