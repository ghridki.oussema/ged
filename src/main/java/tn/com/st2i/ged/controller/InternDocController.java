package tn.com.st2i.ged.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import tn.com.st2i.ged.model.Document;
import tn.com.st2i.ged.model.RefDocument;
import tn.com.st2i.ged.tools.model.SendObject;
import tn.com.st2i.ged.service.IDocService;
import tn.com.st2i.ged.service.IDocumentService;
import tn.com.st2i.ged.service.ISendWsService;
import tn.com.st2i.ged.tools.ConstanteWs;

@RestController
@RequestMapping("/intern/document")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class InternDocController {

	private static final Logger logger = LogManager.getLogger(InternDocController.class);

	@Autowired
	private ISendWsService sendWsService;

	@Autowired
	private IDocService docService;

	@Autowired
	private IDocumentService documentService;

	@Operation(summary = "Download file")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus parametre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@GetMapping(value = "/downloadFile")
	public ResponseEntity<?> getfileToDownloadWs(HttpServletRequest request,
			@RequestParam(name = "nodeRef", required = true) String nodeRef) {
		File file = null;
		try {

			SendObject so = docService.downloadFileFromAlfrByNodeRef(nodeRef);
			file = (File) so.getPayload();
			so.setHttp(HttpStatus.OK);

			InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
			return ResponseEntity.ok()
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"")
					.contentLength(file.length()).contentType(MediaType.parseMediaType("application/octet-stream"))
					.body(resource);
		} catch (Exception argEx) {
			logger.error("Error InternDocController in method getfileToDownloadWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		} finally {
			file.delete();
		}
	}

	@Operation(summary = "Download folder")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus parametre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@GetMapping(value = "/zip-download", produces = "application/zip")
	public ResponseEntity<?> getFolderToDownloadWs(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(name = "nodeRef", required = true) String nodeRef) {

		try {

			List<RefDocument> list = documentService.getListDocByRefDoss(nodeRef);
			List<File> files = new ArrayList<>();
			for (RefDocument refDocument : list) {
				SendObject so = docService.downloadFileFromAlfrByNodeRef(refDocument.getNodeRef());
				File file = (File) so.getPayload();
				so.setHttp(HttpStatus.OK);
				files.add(file);
			}

			ServletOutputStream out = response.getOutputStream();
			ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(out));
			for (File file : files) {

				System.out.println("Adding file " + file.getName());
				zos.putNextEntry(new ZipEntry(file.getName()));

				// Get the file
				FileInputStream fis = null;
				try {
					fis = new FileInputStream(file);

				} catch (FileNotFoundException fnfe) {

					zos.write(("ERROR: Could not find file " + file.getName()).getBytes());
					zos.closeEntry();
					System.out.println("Could not find file " + file.getAbsolutePath());
					continue;
				}

				BufferedInputStream fif = new BufferedInputStream(fis);

				// Write the contents of the file
				int data = 0;
				while ((data = fif.read()) != -1) {
					zos.write(data);
				}
				fif.close();

				zos.closeEntry();
				System.out.println("Finished adding file " + file.getName());
			}

			zos.close();

		} catch (Exception argEx) {
			logger.error("Error InternDocController in method getFolderToDownloadWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
		return null;
	}

	@PostMapping(value = "/savaFile", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> saveFileWs(HttpServletRequest request,
			@RequestParam(name = "filedata", required = false) List<MultipartFile> filedata,
			@RequestParam(name = "document", required = true) String data) throws IOException {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			mapper.setVisibility(
					VisibilityChecker.Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY));
			Document document = mapper.readValue(data, Document.class);
			document.setListFile(filedata);
			return sendWsService.sendResultPublic(request, documentService.saveFile(document, document.getListFile()));
		} catch (Exception argEx) {
			logger.error("Error DocumentController in method saveFileWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Delete file by nodRef")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus parametre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@DeleteMapping(value = "file/{nodeRef}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteFileWs(HttpServletRequest request,
			@PathVariable(value = "nodeRef", required = true) String nodeRef) {
		try {
			return sendWsService.sendResultPublic(request, docService.deleteFileWs(nodeRef));
		} catch (Exception argEx) {
			logger.error("Error InternDocController in method deleteFileWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Delete list of files by nodRef")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus parametre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@DeleteMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteListFileWs(HttpServletRequest request, @RequestBody List<String> list) {
		try {
			return sendWsService.sendResultPublic(request, docService.deleteListFileWs(list));
		} catch (Exception argEx) {
			logger.error("Error InternDocController in method deleteListFileWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Delete folder by nodRef")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus parametre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@DeleteMapping(value = "folder/{nodeRef}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteFolderWs(HttpServletRequest request,
			@PathVariable(value = "nodeRef", required = true) String nodeRef) {
		try {
			return sendWsService.sendResultPublic(request, docService.deleteFolder(nodeRef));
		} catch (Exception argEx) {
			logger.error("Error InternDocController in method deleteFileWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Get list of files by nodRef ")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus parametre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@GetMapping(value = "/files")
	public ResponseEntity<?> getFilesFromFolderWs(HttpServletRequest request,
			@RequestParam(name = "nodeRef", required = true) String nodRef) throws IOException {
		try {

			return sendWsService.sendResultPublic(request, documentService.handleListDocByRefDoss(nodRef));
		} catch (Exception argEx) {
			logger.error("Error DocumentController in method getFilesFromFolderWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

}
